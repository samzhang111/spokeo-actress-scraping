class CreateCelebtwits < ActiveRecord::Migration
  def change
    create_table :celebtwits do |t|
      t.string :name
      t.string :acct

      t.timestamps
    end
  end
end
