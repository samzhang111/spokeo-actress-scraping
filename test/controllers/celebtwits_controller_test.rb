require 'test_helper'

class CelebtwitsControllerTest < ActionController::TestCase
  setup do
    @celebtwit = celebtwits(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:celebtwits)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create celebtwit" do
    assert_difference('Celebtwit.count') do
      post :create, celebtwit: { acct: @celebtwit.acct, name: @celebtwit.name }
    end

    assert_redirected_to celebtwit_path(assigns(:celebtwit))
  end

  test "should show celebtwit" do
    get :show, id: @celebtwit
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @celebtwit
    assert_response :success
  end

  test "should update celebtwit" do
    patch :update, id: @celebtwit, celebtwit: { acct: @celebtwit.acct, name: @celebtwit.name }
    assert_redirected_to celebtwit_path(assigns(:celebtwit))
  end

  test "should destroy celebtwit" do
    assert_difference('Celebtwit.count', -1) do
      delete :destroy, id: @celebtwit
    end

    assert_redirected_to celebtwits_path
  end
end
