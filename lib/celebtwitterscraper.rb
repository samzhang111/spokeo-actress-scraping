require 'typhoeus'
require 'nokogiri'
require 'twitter'

=begin

CelebTwitterScraper class

Usage:

cts = CelebTwitterScraper.new
cts.populate_database

Then, call cts.scrape every minute

=end

class CelebTwitterScraper
    def initialize
        @client ||= self.connect_twitter
    end

    def connect_twitter
        client = Twitter::REST::Client.new do |config|
            config.consumer_key = Celebtwitters.twitterkey
            config.consumer_secret = Celebtwitters.twittersecret
            config.access_token = Celebtwitters.twitteraccesskey
            config.access_token_secret = Celebtwitters.twitteraccesssecret
        end
    end
=begin

    Scrapes the Wikipedia mobile page for American film actresses.
    Stores actresses into a database.

    Note! This depends on the formatting style of the actress names to remain like it was on Feb 21st, 2014.
          A copy of the source code is included for reference at lib/wiki.html

          But as of 2/21/14, living actresses either have "born" in their description, or lack a dash.

=end
    def populate_names
        request = Typhoeus.get('en.m.wikipedia.org/wiki/List_of_American_film_actresses')
        n = Nokogiri::HTML.parse(request.body)
        valid = []
        
        # xpath for actress links
        names = n.xpath('//div[@class="div-col columns column-width"]/ul/li').map {|li|
            name = li.at('a')
            ct = Celebtwit.new
            ct.name = name.text
            ct.acct = ''
            additional = li.xpath('text()').text
            
            # Note: the '–' here is a dash, not a hyphen
            if additional.include? 'born' or not additional.include? '–'
                valid.push(name.text)
                ct.acct = 'scrape'
            end
            ct.save
        }
        valid
    end

=begin

    Input: celebrity name
    Returns: celebrity twitter url, if they have a verified account
                      empty string, if not

=end

    def search_twitter_for_name (name)
        @client ||= self.connect_twitter
        url = ''
        results = @client.user_search(name)
        if not results.empty?
            verified = results.select{ |result| result['verified'] }
            if not verified.empty?
                url = 'https://twitter.com/' + verified[0].screen_name
            end
        end
        return url
    end

=begin

Scrapes n celebrity twitters from the database every minute.

=end
    def scrape
        cts = Celebtwit.where('acct = "scrape"').limit(8)
        cts.each do |ct|
            puts "==> Scraping #{ct.id}"
            url = search_twitter_for_name(ct.name)
            ct.acct = url
            ct.save
            puts "==> Added #{ct.name} - #{url} to database"
        end
    end

end

