require 'typhoeus'
require 'nokogiri'
require 'twitter'
require 'celebtwitterscraper'

namespace :scrape do
    
    desc 'scrapes living actresses off of wikipedia'
    task :init => :environment do
        s = CelebTwitterScraper.new
        s.populate_names
    end

    desc 'updates db with actress twitter handles'
    task :run => :environment do
        s = CelebTwitterScraper.new
        s.scrape
    end
    
    desc 'test that the Twitter API is returning correct results'
    task :test => :environment do
        puts 'Twitter API test. Note: this test will invoke two queries to the Twitter API'
        s = CelebTwitterScraper.new
        puts 'Test 1. Halle Berry: No verified accounts, should return empty string'
        test1 = s.search_twitter_for_name('Halle Berry') 
        raise "Test 1. Wrong result: #{test1}" unless test1.empty?
        puts '        Success!'
         
        puts 'Test 2. Justin Bieber: Verified accounts, should return his Twitter URL'
        test2 = s.search_twitter_for_name('Justin Bieber') 
        raise "Test 2.Wrong result: #{test2}" unless test2=='https://twitter.com/justinbieber'
        puts '        Success!'
    end

end

