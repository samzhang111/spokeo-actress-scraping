Rails Web Scraper
================

Extracts actress names from Wikipedia and finds corresponding twitter accounts.

### Run

1. 'bundle install'
2. set config options 
2. 'rake db:migrate'
3. 'rake scrape:init'
4. then from rails root directory, 'whenever -w'

### Files

config/app.yml -> twitterkey = twitter API consumer key
                  twittersecret = twitter API secret key
                  twitteraccesskey = OAuth access key
                  twitteraccesssecret = OAuth secret key

(cp config/app\_example.yml config/app.yml)

config/database.yml -> set up rails database
(cp config/database\_example.yml config/database.yml)

lib/celebtwitterscraper.rb:

- contains main CelebTwitterScraper class
- populate\_names -> scrapes names off Wikipedia
- scrape -> queries batch of names on Twitter

### Implementation details

This scraper makes the following assumptions:

- Only living actresses have twitters.
- Only verified accounts will be scraped
- The Twitter API can only handle 150 queries per 15 minutes (10/min)

The whenever gem is used to schedule a cron job that calls 'rake scrape:run' every minute. scrape:run is hard-coded to run 8 times (to allow leeway for tests and restarts)

Since the Twitter API is rate limited and the scraping is single threaded, the database itself is used as the queue.

### Results

Collected 686 Twitter accounts, out of 1587 names.
