json.array!(@celebtwits) do |celebtwit|
  json.extract! celebtwit, :id, :name, :acct
  json.url celebtwit_url(celebtwit, format: :json)
end
