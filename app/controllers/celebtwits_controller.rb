class CelebtwitsController < ApplicationController
  before_action :set_celebtwit, only: [:show, :edit, :update, :destroy]

  # GET /celebtwits
  # GET /celebtwits.json
  def index
    @celebtwits = Celebtwit.all
  end

  # GET /celebtwits/1
  # GET /celebtwits/1.json
  def show
  end

  # GET /celebtwits/new
  def new
    @celebtwit = Celebtwit.new
  end

  # GET /celebtwits/1/edit
  def edit
  end

  # POST /celebtwits
  # POST /celebtwits.json
  def create
    @celebtwit = Celebtwit.new(celebtwit_params)

    respond_to do |format|
      if @celebtwit.save
        format.html { redirect_to @celebtwit, notice: 'Celebtwit was successfully created.' }
        format.json { render action: 'show', status: :created, location: @celebtwit }
      else
        format.html { render action: 'new' }
        format.json { render json: @celebtwit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /celebtwits/1
  # PATCH/PUT /celebtwits/1.json
  def update
    respond_to do |format|
      if @celebtwit.update(celebtwit_params)
        format.html { redirect_to @celebtwit, notice: 'Celebtwit was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @celebtwit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /celebtwits/1
  # DELETE /celebtwits/1.json
  def destroy
    @celebtwit.destroy
    respond_to do |format|
      format.html { redirect_to celebtwits_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_celebtwit
      @celebtwit = Celebtwit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def celebtwit_params
      params.require(:celebtwit).permit(:name, :acct)
    end
end
